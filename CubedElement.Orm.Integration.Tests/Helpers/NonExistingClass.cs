using CubedElement.Orm.Contracts;

namespace CubedElement.Orm.Integration.Tests.Helpers
{
    public class NonExistingClass : Entity<int>
    {
        public string WhoCares { get; set; }
    }
}