using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CubedElement.Orm.Contracts;

namespace CubedElement.Orm.Integration.Tests.Helpers.Models
{
    [Table("Keyword")]
    public class Keyword : Entity<int>, IEntity<int>
    {
        public DateTime CreatedDate { get; set; }

        [Required]
        [StringLength(30)]
        public string Word { get; set; }
    }
}
