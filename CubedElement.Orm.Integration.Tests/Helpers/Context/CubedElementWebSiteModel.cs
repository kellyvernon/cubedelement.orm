using CubedElement.Orm.Integration.Tests.Helpers.Models;
using Microsoft.EntityFrameworkCore;

namespace CubedElement.Orm.Integration.Tests.Helpers.Context
{
    public partial class CubedElementWebSiteModel : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=localhost;Database=deploy_cubedelement_site;Trusted_Connection=True;User ID=webaccount;Password=webaccount");
        }

        public virtual DbSet<Keyword> Keywords { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Keyword>()
                .Property(e => e.Word)
                .IsUnicode(false);
        }
    }
}
