﻿using System;
using System.Text;
using CubedElement.Logging.Contracts;

namespace CubedElement.Orm.Integration.Tests.Helpers
{
    internal class DumpLogger : ILogger
    {
        private StringBuilder log;

        public DumpLogger(StringBuilder log)
        {
            this.log = log;
        }

        public void Trace(string message)
        {
            throw new NotImplementedException();
        }

        public void Trace(Exception exception)
        {
            throw new NotImplementedException();
        }

        public void Trace(string message, Exception exception)
        {
            throw new NotImplementedException();
        }

        public void Info(string message)
        {
            log.AppendLine(message);
        }

        public void Info(Exception exception)
        {
            throw new NotImplementedException();
        }

        public void Info(string message, Exception exception)
        {
            throw new NotImplementedException();
        }

        public void Warn(string message)
        {
            log.AppendLine(string.Format("WARN {0}", message));
        }

        public void Warn(Exception exception)
        {
            log.AppendLine(string.Format("WARN {0}", exception.ToString()));
        }

        public void Warn(string message, Exception exception)
        {
            log.AppendLine(string.Format("WARN {0}{1}", message, exception.ToString()));
        }

        public void Debug(string message)
        {
            log.AppendLine(string.Format("DEBUG {0}", message));
        }

        public void Debug(Exception exception)
        {
            log.AppendLine(string.Format("DEBUG {0}", exception));
        }

        public void Debug(string message, Exception exception)
        {
            log.AppendLine(string.Format("DEBUG {0}{1}", message, exception.ToString()));
        }

        public void Error(string message)
        {
            log.AppendLine(string.Format("ERROR {0}", message));
        }

        public void Error(Exception exception)
        {
            log.AppendLine(string.Format("ERROR {0}", exception.ToString()));
        }

        public void Error(string message, Exception exception)
        {
            log.AppendLine(string.Format("ERROR {0}{1}", message, exception));
        }

        public void Fatal(string message)
        {
            throw new NotImplementedException();
        }

        public void Fatal(Exception exception)
        {
            throw new NotImplementedException();
        }

        public void Fatal(string message, Exception exception)
        {
            throw new NotImplementedException();
        }

        public void Log(LogType logType, string message)
        {
            log.AppendLine(string.Format("{0} {1}", Enum.GetName(typeof(LogType), logType), message));
        }

        public void Log(LogType logType, Exception exception)
        {
            log.AppendLine(string.Format("{0} {1}", Enum.GetName(typeof(LogType), logType), exception.ToString()));
        }

        public void Log(LogType logType, string message, Exception exception)
        {
            throw new NotImplementedException();
        }
    }
}