﻿using CubedElement.Orm.Integration.Tests.Helpers.Models;
using Microsoft.EntityFrameworkCore;

namespace CubedElement.Orm.Tests
{
    public class ElementSiteEntities : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=localhost;Database=deploy_cubedelement_site;Trusted_Connection=True;");
        }

        public DbSet<Keyword> Pages { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Keyword>()
                .Property(e => e.Word)
                .IsUnicode(false);
        }
    }
}