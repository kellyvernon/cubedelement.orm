﻿using System;
using System.Collections.Generic;
using System.Linq;
using CubedElement.Orm.Integration.Tests.Helpers.Context;
using CubedElement.Orm.Integration.Tests.Helpers.Models;
using Xunit;

namespace CubedElement.Orm.Integration.Tests.WhenWorkingWIthData.AndUsingDefaultEntityFramework
{
    public class AndGettingAllPages
    {
        public AndGettingAllPages()
        {
            _context = new CubedElementWebSiteModel();
            _pages = _context.Keywords.ToList();
        }

        private readonly CubedElementWebSiteModel _context;
        private IList<Keyword> _pages;


        public void Dispose()
        {
            _context.Dispose();
            _pages = null;
        }

        [Fact]
        public void ItShouldContainContent()
        {
            foreach (var page in _pages)
            {
                Assert.NotEqual(page.Word, null);
                Assert.NotEqual(page.Word, string.Empty);
            }
        }

        [Fact]
        public void ItShouldReturnFullCollection()
        {
            Assert.Equal(_pages.Count > 0, true);

            foreach (var page in _pages)
            {
                Assert.NotEqual(page.Word, null);
                Assert.NotEqual(page.Word, string.Empty);
            }
        }


        //[Fact]
        public void ItShouldThrowExceptionWhenContextIsClosed()
        {
            _context.Dispose();

            foreach (var page in _pages)
                Assert.Throws<ObjectDisposedException>(() =>
                {
                    var bla = page;
                });
        }
    }
}