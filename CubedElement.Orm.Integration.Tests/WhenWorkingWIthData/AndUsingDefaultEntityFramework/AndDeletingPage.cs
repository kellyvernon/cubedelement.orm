﻿using System;
using System.Linq;
using CubedElement.Orm.Integration.Tests.Helpers.Context;
using Xunit;


namespace CubedElement.Orm.Integration.Tests.WhenWorkingWIthData.AndUsingDefaultEntityFramework
{
    
    public class AndDeletingPage
    {
        private CubedElementWebSiteModel _context;
        
        public AndDeletingPage()
        {
            _context = new CubedElementWebSiteModel();
        }

        
        public void Dispose()
        {
            _context.Dispose();
        }

        [Fact]
        public void ItShouldRemoveExistingPage()
        {
            try
            {
                using (var dbContextTransaction = _context.Database.BeginTransaction())
                {
                    var pages = _context.Keywords.ToList();

                    var pagesCountBefore = pages.Count;

                    // sql hates min date time, so use its lowest allowed or current date
                    var currentDate = DateTime.Now;

                    var pg = pages[new Random().Next(0, pages.Count)];

                    pages.Remove(pg);

                    _context.SaveChanges();

                    Assert.Equal(pagesCountBefore - 1, pages.Count);

                    dbContextTransaction.Rollback();
                }
            }
            catch (Exception e)
            {
                Assert.True(false, e.Message);
            }
        }
    }
}