﻿using System;
using System.Linq;
using CubedElement.Orm.Integration.Tests.Helpers.Context;
using CubedElement.Orm.Integration.Tests.Helpers.Models;
using Xunit;


namespace CubedElement.Orm.Integration.Tests.WhenWorkingWIthData.AndUsingDefaultEntityFramework
{
    
    public class AndSavingNewPage
    {
        private CubedElementWebSiteModel _context;

        public AndSavingNewPage()
        {
            _context = new CubedElementWebSiteModel();
        }

        
        public void Dispose()
        {
            _context.Dispose();
        }

        [Fact]
        public void ItShouldAddANewPage()
        {
            try
            {
                using (var dbContextTransaction = _context.Database.BeginTransaction())
                {
                    var pagesCountBefore = _context.Keywords.Count();

                    // sql hates min date time, so use its lowest allowed or current date
                    var currentDate = DateTime.Now;

                    var pg = new Keyword
                    {
                        CreatedDate = currentDate,
                        Word = Guid.NewGuid().ToString("N").Substring(0, 30)
                    };

                    _context.Keywords.Add(pg);

                    _context.SaveChanges();

                    Assert.Equal(pagesCountBefore + 1, _context.Keywords.Count());

                    dbContextTransaction.Rollback();
                }
            }
            catch (Exception e)
            {
                Assert.True(false, e.Message);
            }
        }
    }
}