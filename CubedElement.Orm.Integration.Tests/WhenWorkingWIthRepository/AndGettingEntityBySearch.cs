﻿using System;
using System.Collections.Generic;
using System.Linq;
using CubedElement.Logging.Contracts;
using CubedElement.Orm.EntityFramework;
using CubedElement.Orm.Integration.Tests.Helpers;
using CubedElement.Orm.Integration.Tests.Helpers.Context;
using CubedElement.Orm.Integration.Tests.Helpers.Models;
using Moq;
using Xunit;

namespace CubedElement.Orm.Integration.Tests.WhenWorkingWIthRepository
{
    
    public class AndGettingEntityBySearch
    {
        private CubedElementWebSiteModel _context;
        private Random _rnd;
        List<Keyword> _pages;
        private Mock<ILogger> _logger;

        public AndGettingEntityBySearch()
        {
            _rnd = new Random(1);

            _context = new CubedElementWebSiteModel();

            _logger = new Mock<ILogger>();

            using (_context)
            {
                _pages = _context.Keywords.ToList();
            }
        }

        
        public void Dispose()
        {
            _context.Dispose();
        }

        [Fact]
        public void AndContainsContent()
        {
            using (var repository = new GenericReadWriteRepository<Keyword, int>(new CubedElementWebSiteModel(), _logger.Object))
            {
                var page = _pages[_rnd.Next(0, _pages.Count - 1)];

                var pages = repository.Where(x => x.Word.Contains(page.Word)).Result;

                Assert.NotEqual(pages, null);

                Assert.Equal(pages.Count >= 1, true);
            }
        }

        [Fact]
        public void AndNoMatchingContentItShouldReturnEmptyCollection()
        {
            using (var repository = new GenericReadWriteRepository<Keyword, int>(new CubedElementWebSiteModel(), _logger.Object))
            {
                var pages = repository.Where(x => x.Word.Contains(Guid.NewGuid().ToString())).Result;

                Assert.Equal(pages.Count, 0);
            }
        }

        [Fact]
        public void AndEntityNotExistsItShouldReturnEmptyCollection()
        {
            using (var repository = new GenericReadWriteRepository<NonExistingClass, int>(new CubedElementWebSiteModel(), _logger.Object))
            {
                var nonExistingClasses = repository.Where(x => x.WhoCares.Contains("party")).Result;

                Assert.Equal(nonExistingClasses.Count, 0);
            }
        }
    }
}