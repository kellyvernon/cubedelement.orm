﻿using System;
using System.Collections.Generic;
using System.Linq;
using CubedElement.Logging.Contracts;
using CubedElement.Orm.EntityFramework;
using CubedElement.Orm.Integration.Tests.Helpers;
using CubedElement.Orm.Integration.Tests.Helpers.Context;
using CubedElement.Orm.Integration.Tests.Helpers.Models;
using Moq;
using Xunit;

namespace CubedElement.Orm.Integration.Tests.WhenWorkingWIthRepository
{
    
    public class AndGettingEntity
    {
        private CubedElementWebSiteModel _context;
        private Random _rnd;
        List<Keyword> _pages;
        private Mock<ILogger> _logger;
        
        public AndGettingEntity()
        {
            _rnd = new Random(1);

            _context = new CubedElementWebSiteModel();

            _logger = new Mock<ILogger>();

            using (_context)
            {
                _pages = _context.Keywords.ToList();
            }
        }

        
        public void Dispose()
        {
            _context.Dispose();
        }

        [Fact]
        public void AndEntityExistsItShouldReturnById()
        {
            using (var repository = new GenericReadWriteRepository<Keyword, int>(new CubedElementWebSiteModel(), _logger.Object))
            {
                var page = repository.GetById(_pages[_rnd.Next(0, _pages.Count)].Id);

                Assert.NotEqual(page, null);
            }
        }

        [Fact]
        public void AndNoMatchingIdItShouldReturnById()
        {
            using (var repository = new GenericReadWriteRepository<Keyword, int>(new CubedElementWebSiteModel(), _logger.Object))
            {
                var page = repository.GetById(_pages.Count + 1).Result;

                Assert.Equal(page, null);
            }
        }

        [Fact]
        public void AndEntityNotExistsItShouldReturnById()
        {
            using (var repository = new GenericReadWriteRepository<NonExistingClass, int>(new CubedElementWebSiteModel(), _logger.Object))
            {
                var page = repository.GetById(_pages.Count + 1).Result;

                Assert.Equal(page, null);
            }
        }
    }
}