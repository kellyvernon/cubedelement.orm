﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CubedElement.Logging.Contracts;
using CubedElement.Orm.EntityFramework;
using CubedElement.Orm.Integration.Tests.Helpers;
using CubedElement.Orm.Integration.Tests.Helpers.Context;
using CubedElement.Orm.Integration.Tests.Helpers.Models;
using Moq;
using Xunit;

namespace CubedElement.Orm.Integration.Tests.WhenWorkingWIthRepository
{
    
    public class AndDeletingEntity
    {
        private CubedElementWebSiteModel _context;
        private List<Keyword> _keywords;

        private Keyword _keyword;
        private Mock<ILogger> _logger;

        public AndDeletingEntity()
        {
            _context = new CubedElementWebSiteModel();

            _logger = new Mock<ILogger>();

            using (_context)
            {
                _keywords = _context.Keywords.ToList();

                _keyword = _keywords[new Random().Next(0, _keywords.Count - 1)];

                Assert.NotEqual(_keyword, null);
            }
        }

        
        public void Dispose()
        {
            _context.Dispose();
        }

        [Fact]
        public void AndEntityDoesNotExist()
        {
            var stringBuilder = new StringBuilder();

            var dumpLogger = new DumpLogger(stringBuilder);

            using (var repositoryAll = new GenericReadWriteRepository<Keyword, int>(new CubedElementWebSiteModel(), dumpLogger))
            {
                var keywords = repositoryAll.GetAll().Result;
                var count = keywords.Count;

                using (var repositoryRemove = new GenericReadWriteRepository<Keyword, int>(new CubedElementWebSiteModel(), dumpLogger))
                {
                    var keywordToRemove = new Keyword
                    {
                        Id = _keywords.Count + 2
                    };

                    repositoryRemove.Remove(keywordToRemove).Wait();

                    using (var repositoryAllVerify = new GenericReadWriteRepository<Keyword, int>(new CubedElementWebSiteModel(), dumpLogger))
                    {
                        var newCount = repositoryAllVerify.GetAll().Result.Count;

                        Assert.Equal(newCount, count);
                    }
                }
            }
        }

        [Fact]
        public void AndEntityDoesExist()
        {
            var stringBuilder = new StringBuilder();

            var dumpLogger = new DumpLogger(stringBuilder);

            using (var repositoryAdd = new GenericReadWriteRepository<Keyword, int>(new CubedElementWebSiteModel(), dumpLogger))
            {
                _keyword = new Keyword
                {
                    Word = $"poo{new Random().Next(0, 999)}",
                    CreatedDate = DateTime.Now
                };

                var e = repositoryAdd.Add(_keyword).Result;

                using (var repositoryRemove = new GenericReadWriteRepository<Keyword, int>(new CubedElementWebSiteModel(), dumpLogger))
                {
                    repositoryRemove.Remove(e).Wait();

                    using (var repositoryAll = new GenericReadWriteRepository<Keyword, int>(new CubedElementWebSiteModel(), dumpLogger))
                    {
                        var newCount = repositoryAll.GetAll().Result.Count;

                        Assert.Equal(newCount, _keywords.Count);
                    }
                }
            }
        }

        [Fact]
        public void AndEntityNotExistsItShouldNotThrowException()
        {
            _context = new CubedElementWebSiteModel();

            using (var repository = new GenericReadWriteRepository<NonExistingClass, int>(new CubedElementWebSiteModel(), _logger.Object))
            {
                Record.Exception(() => repository.Remove(new NonExistingClass()).Wait());
            }
        }
    }
}