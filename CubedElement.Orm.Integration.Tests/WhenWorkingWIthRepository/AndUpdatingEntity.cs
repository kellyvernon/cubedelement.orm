﻿using System;
using System.Collections.Generic;
using System.Linq;
using CubedElement.Logging.Contracts;
using CubedElement.Orm.Contracts;
using CubedElement.Orm.EntityFramework;
using CubedElement.Orm.Integration.Tests.Helpers;
using CubedElement.Orm.Integration.Tests.Helpers.Context;
using CubedElement.Orm.Integration.Tests.Helpers.Models;
using Moq;
using Xunit;

namespace CubedElement.Orm.Integration.Tests.WhenWorkingWIthRepository
{
    
    public class AndUpdatingEntity
    {
        private CubedElementWebSiteModel _context;
        private List<Keyword> _pages;

        private Keyword _page;
        private Mock<ILogger> _logger;

        public AndUpdatingEntity()
        {
            _context = new CubedElementWebSiteModel();

            _logger = new Mock<ILogger>();

            var currentDate = DateTime.Now;

            _page = new Keyword
            {
                CreatedDate = currentDate,
                Word = Guid.NewGuid().ToString()
            };

            using (_context)
            {
                _pages = _context.Keywords.ToList();
            }
        }

        
        public void Dispose()
        {
            _context.Dispose();
        }

        [Fact]
        public void AndEntityDoesNotExist()
        {
            _context = new CubedElementWebSiteModel();

            int newCount;

            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                IWriteRepository<Keyword, int> repository = new GenericReadWriteRepository<Keyword, int>(_context, _logger.Object);

                repository.Update(_page);

                newCount = _context.Keywords.Count();

                dbContextTransaction.Rollback();
            }

            Assert.Equal(_pages.Count, newCount);
        }

        [Fact]
        public void AndEntityDoesExist()
        {
            _context = new CubedElementWebSiteModel();

            int newCount;

            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                IWriteRepository<Keyword, int> repository = new GenericReadWriteRepository<Keyword, int>(_context, _logger.Object);

                repository.Update(_pages[0]);

                newCount = _context.Keywords.Count();

                dbContextTransaction.Rollback();
            }

            Assert.Equal(_pages.Count, newCount);
        }

        [Fact]
        public void AndEntityNotExistsItShouldReturnById()
        {
            _context = new CubedElementWebSiteModel();

            IReadWriteRepository<NonExistingClass, int> repository = new GenericReadWriteRepository<NonExistingClass, int>(_context, _logger.Object);

            Record.Exception(() => repository.Update(new NonExistingClass()).Wait());
        }
    }
}