﻿using CubedElement.Logging.Contracts;
using CubedElement.Orm.EntityFramework;
using CubedElement.Orm.Integration.Tests.Helpers;
using CubedElement.Orm.Integration.Tests.Helpers.Context;
using CubedElement.Orm.Integration.Tests.Helpers.Models;
using Moq;
using Xunit;

namespace CubedElement.Orm.Integration.Tests.WhenWorkingWIthRepository.AndGettingEntities
{
    
    public class AndContext
    {
        private CubedElementWebSiteModel _context;
        private Mock<ILogger> _logger;

        public AndContext()
        {
            _context = new CubedElementWebSiteModel();

            _logger = new Mock<ILogger>();
        }

        
        public void Dispose()
        {
            _context.Dispose();
        }

        [Fact]
        public void AndEntitiesExist()
        {
            var repository = new GenericReadWriteRepository<Keyword, int>(_context, _logger.Object);

            var page = repository.GetAll();

            Assert.NotEqual(page, null);
        }

        [Fact]
        public void AndEntityNotExistsItShouldReturnEmptyCollection()
        {
            var repository = new GenericReadWriteRepository<NonExistingClass, int>(_context, _logger.Object);

            var page = repository.GetAll().Result;

            Assert.NotEqual(page, null);

            Assert.Equal(page.Count, 0);
        }
    }
}