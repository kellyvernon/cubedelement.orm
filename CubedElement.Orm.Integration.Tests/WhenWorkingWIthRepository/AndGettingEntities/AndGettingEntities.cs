﻿using CubedElement.Logging.Contracts;
using CubedElement.Orm.EntityFramework;
using CubedElement.Orm.Integration.Tests.Helpers;
using CubedElement.Orm.Integration.Tests.Helpers.Context;
using CubedElement.Orm.Integration.Tests.Helpers.Models;
using Moq;
using Xunit;

namespace CubedElement.Orm.Integration.Tests.WhenWorkingWIthRepository.AndGettingEntities
{
    
    public class AndGettingEntities
    {
        private CubedElementWebSiteModel _context;
        private Mock<ILogger> _logger;
        
        public AndGettingEntities()
        {
            _context = new CubedElementWebSiteModel();

            _logger = new Mock<ILogger>();
        }

        
        public void Dispose()
        {
            _context.Dispose();
        }

        [Fact]
        public void AndEntitiesExist()
        {
            using (var repository = new GenericReadWriteRepository<Keyword, int>(new CubedElementWebSiteModel(), _logger.Object))
            {
                var page = repository.GetAll();

                Assert.NotEqual(page, null);
            }
        }

        [Fact]
        public void AndEntityNotExistsItShouldReturnEmptyCollection()
        {
            using (var repository = new GenericReadWriteRepository<NonExistingClass, int>(new CubedElementWebSiteModel(), _logger.Object))
            {
                var page = repository.GetAll().Result;

                Assert.NotEqual(page, null);

                Assert.Equal(page.Count, 0);
            }
        }
    }
}