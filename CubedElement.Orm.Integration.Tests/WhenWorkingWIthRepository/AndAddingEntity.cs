﻿using System;
using System.Collections.Generic;
using System.Text;
using CubedElement.Logging.Contracts;
using CubedElement.Orm.EntityFramework;
using CubedElement.Orm.Integration.Tests.Helpers;
using CubedElement.Orm.Integration.Tests.Helpers.Context;
using CubedElement.Orm.Integration.Tests.Helpers.Models;
using Xunit;


namespace CubedElement.Orm.Integration.Tests.WhenWorkingWIthRepository
{
    
    public class AndAddingEntity
    {
        public AndAddingEntity()
        {
            _stringBuilder = new StringBuilder();
            _logger = new DumpLogger(_stringBuilder);

            var currentDate = DateTime.Now;

            _word = new Keyword
            {
                CreatedDate = currentDate,
                Word = Guid.NewGuid().ToString("N").Substring(0, 10)
            };
        }

        private Keyword _word;
        private ILogger _logger;

        private StringBuilder _stringBuilder;

        [Fact]
        public void AndEntityDoesExist()
        {
            IList<Keyword> keywords;

            using (var repositoryAll = new GenericReadWriteRepository<Keyword, int>(new CubedElementWebSiteModel(), _logger))
            {
                keywords = repositoryAll.GetAll().Result;
            }

            using (var repository = new GenericReadWriteRepository<Keyword, int>(new CubedElementWebSiteModel(), _logger))
            {
                repository.Add(keywords[0]).Wait();
            }

            using (var repositoryAll = new GenericReadWriteRepository<Keyword, int>(new CubedElementWebSiteModel(), _logger))
            {
                var newCount = repositoryAll.GetAll().Result.Count;

                Assert.Equal(keywords.Count, newCount);
            }
        }

        [Fact]
        public void AndEntityDoesNotExist()
        {
            int pageCount;
            _stringBuilder.AppendLine($"Word: {_word.Word}-- Id:{_word.Id}-- isNew: {_word.IsNew}");

            using (var repositoryAll = new GenericReadWriteRepository<Keyword, int>(new CubedElementWebSiteModel(), _logger))
            {
                pageCount = repositoryAll.GetAll().Result.Count;
            }

            using (var repository = new GenericReadWriteRepository<Keyword, int>(new CubedElementWebSiteModel(), _logger))
            {
                var a = repository.Add(_word).Result;
                _stringBuilder.AppendLine($"AWord: {a.Word}-- AId:{a.Id}-- AIsNew: {a.IsNew}");
            }

            using (var repositoryAllResult = new GenericReadWriteRepository<Keyword, int>(new CubedElementWebSiteModel(), _logger))
            {
                var result = repositoryAllResult.GetAll().Result;
                var newCount = result.Count;

                using (var repoWhere = new GenericReadWriteRepository<Keyword, int>(new CubedElementWebSiteModel(), _logger))
                {
                    var items = repoWhere.GetAll().Result;

                    foreach (var keyword in items)
                    {
                        _stringBuilder.AppendLine($"key: {keyword.Word}{keyword.Id} ");
                    }

                    _stringBuilder.AppendLine("pageCount: " + pageCount).AppendLine("newCount: " + newCount);
                    Assert.Equal(pageCount + 1, newCount);
                }
            }
        }

        [Fact]
        public void AndEntityNotExistsItShouldReturnById()
        {
            using (var repository = new GenericReadWriteRepository<NonExistingClass, int>(new CubedElementWebSiteModel(), _logger))
            {
                Record.Exception(() => repository.Add(new NonExistingClass()).Wait());
            }
        }
    }
}