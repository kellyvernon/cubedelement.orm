﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CubedElement.Orm.Contracts
{
    /// <summary>
    /// Methods used to only retrieve data from the database
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TU"></typeparam>
    public interface IReadRepository<T, TU> : IDisposable where T : class, IEntity<TU> where TU : IEquatable<TU>
    {
        /// <summary>
        /// Using the Identifier to retrieve information from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<T> GetById(TU id);

        /// <summary>
        /// Retrieve all items in the database related to this entity
        /// </summary>
        /// <returns></returns>
        Task<IList<T>> GetAll();

        /// <summary>
        /// Based on the search criteria, it will attempt to find a result. If nothing exists or invalid class, it will return an empty collection and log the exception.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        Task<IList<T>> Where(Expression<Func<T, bool>> predicate);

    }
}