﻿using System;

namespace CubedElement.Orm.Contracts
{
    /// <summary>
    /// Combined representation for both interfaces, <see cref="IReadRepository{T,TU}"/> and <see cref="IWriteRepository{T,TU}"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TU"></typeparam>
    public interface IReadWriteRepository<T, TU> : IReadRepository<T, TU>, IWriteRepository<T, TU> where T : class, IEntity<TU> where TU : IEquatable<TU>
    {
    }
}