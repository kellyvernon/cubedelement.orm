﻿using System;

namespace CubedElement.Orm.Contracts
{
    /// <summary>
    /// The high level common generic entity item
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class Entity<T> : IEntity<T> where T : IEquatable<T>
    {
        /// <summary>
        /// The identifier in this entity in the database
        /// </summary>
        public T Id { get; set; }

        
        /// <summary>
        /// This property determines if the Id is new; meaning if the Id is an int and zero or Id is a Guid and Empty then it will be true. 
        /// This is a great to know when determining whether to add or update
        /// </summary>
        public bool IsNew
        {
            get
            {
                if (IsInt(Id))
                {
                    return IdInt == 0;
                }

                return IsGuid(Id) && IdGuid.Equals(Guid.Empty);
            }
        }

        private int IdInt
        {
            get { 
                return IsInt(Id) 
                    ? Convert.ToInt32(Id.ToString()) 
                    : -1; 
            }
        }

        private Guid IdGuid
        {
            get
            {
                return IsGuid(Id) 
                    ? Guid.Parse(Id.ToString()) 
                    : Guid.Empty;
            }
        }

        private static bool IsType(T argument, Type type)
        {
            return argument.GetType() == type;
        }

        private static bool IsInt(T id)
        {
            return IsType(id, typeof (int));
        }

        private static bool IsGuid(T id)
        {
            return IsType(id, typeof (Guid));
        }
    }
}
