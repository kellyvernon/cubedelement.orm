﻿using System;
using System.Threading.Tasks;

namespace CubedElement.Orm.Contracts
{
    /// <summary>
    /// Methods used to write data to the database
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TU"></typeparam>
    public interface IWriteRepository<T, TU> : IDisposable where T : class, IEntity<TU> where TU : IEquatable<TU>
    {
        /// <summary>
        /// Returns the entity updated with the corresponding Id
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<T> Add(T entity);

        /// <summary>
        /// Updates the entity in the database
        /// </summary>
        /// <param name="entity"></param>
        Task Update(T entity);
        
        /// <summary>
        /// Removes the entity in the database
        /// </summary>
        /// <param name="entity"></param>
        Task Remove(T entity);
    }
}