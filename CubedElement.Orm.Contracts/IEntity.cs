﻿using System;

namespace CubedElement.Orm.Contracts
{
    /// <summary>
    /// The high level common generic entity item
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IEntity<T> where T : IEquatable<T>
    {
        /// <summary>
        /// The identifier in this entity in the database
        /// </summary>
        T Id { get; set; }

        /// <summary>
        /// This property determines if the Id is new; meaning if the Id is an int and zero or Id is a Guid and Empty then it will be true. 
        /// This is a great to know when determining whether to add or update
        /// </summary>
        bool IsNew { get; }
    }
}