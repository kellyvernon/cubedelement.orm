﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CubedElement.Logging.Contracts;
using CubedElement.Orm.Contracts;
using Microsoft.EntityFrameworkCore;

namespace CubedElement.Orm.EntityFramework
{
    /// <summary>
    /// Methods used to retrieve and write data to and from the database
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TU"></typeparam>
    public class GenericReadWriteRepository<T, TU> : IReadWriteRepository<T, TU>, IDisposable
        where T : class, IEntity<TU>
        where TU : IEquatable<TU>
    {
        private readonly DbContext _context;

        private readonly ILogger _logger;

        /// <summary>
        /// Pass in the Entity Framework DbContext you're associating with this and the ILogger too.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        public GenericReadWriteRepository(DbContext context, ILogger logger)
        {
            _context = context;
            _logger = logger;
        }

        /// <summary>
        /// Using the Identifier to retrieve information from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<T> GetById(TU id)
        {
            try
            {
                _logger.WriteDebug("Executing T GetById");

                return await _context.Set<T>().SingleOrDefaultAsync(x => x.Id.Equals(id));
            }
            catch (Exception exception)
            {
                _logger.WriteError(exception);
                return null;
            }
        }

        /// <summary>
        /// Retrieve all items in the database related to this entity
        /// </summary>
        /// <returns></returns>
        public async Task<IList<T>> GetAll()
        {
            try
            {
                _logger.WriteDebug("Executing T GetById");

                return await _context.Set<T>().ToListAsync();
            }
            catch (Exception exception)
            {
                _logger.WriteError(exception);
                return new List<T>();
            }
        }

        /// <summary>
        /// Based on the search criteria, it will attempt to find a result. If nothing exists or invalid class, it will return an empty collection and log the exception.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<IList<T>> Where(Expression<Func<T, bool>> predicate)
        {
            try
            {
                _logger.WriteDebug("Executing T Where");

                return await _context.Set<T>().Where(predicate).ToListAsync();
            }
            catch (Exception exception)
            {
                _logger.WriteError(exception);
                return new List<T>();
            }
        }

        /// <summary>
        /// Returns the entity updated with the corresponding Id
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<T> Add(T entity)
        {
            _logger.WriteDebug("Executing T Add");

            if (!entity.IsNew)
            {
                _logger.WriteWarn($"Entity of type {entity.GetType()} already exists in system");
                return entity;
            }
            
            try
            {
                _logger.WriteDebug("About to add");
                _context.Set<T>().Add(entity);
                _logger.WriteDebug("Just did a set add");

                if (_context.Entry(entity).State == EntityState.Added)
                {
                    _logger.WriteDebug("Executing T Add was in EntityState.Added");
                    await _context.SaveChangesAsync();
                }

                _logger.WriteDebug("returning this");

                return entity;
            }
            catch (DbUpdateException uEx)
            {
                _logger.WriteError(uEx);

                return entity;
            }
            catch (Exception exception)
            {
                _logger.WriteError(exception);

                return entity;
            }
        }

        /// <summary>
        /// Updates the entity in the database
        /// </summary>
        /// <param name="entity"></param>
        public async Task Update(T entity)
        {
            _logger.WriteDebug("Executing void Update");

            if (entity.IsNew)
            {
                _logger.WriteWarn($"Entity of type {entity.GetType()} already exists in system");
                return;
            }
            
            try
            {
                _context.Set<T>().Add(entity);

                if (_context.Entry(entity).State == EntityState.Modified)
                {
                    _logger.WriteDebug("Executing void Update was in EntityState.Modified");
                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception exception)
            {
                _logger.WriteError(exception);
            }
        }

        /// <summary>
        /// Removes the entity in the database
        /// </summary>
        /// <param name="entity"></param>
        public async Task Remove(T entity)
        {
            _logger.WriteDebug("Executing void Remove");

            if (entity.IsNew)
            {
                _logger.WriteWarn(
                    $"Entity of type {entity.GetType()} does not exists in system, so it cannot be deleted");
                return;
            }

            try
            {
                var found = GetById(entity.Id).Result;

                if (found == null)
                {
                    return;
                }

                _context.Set<T>().Remove(found);

                if (_context.Entry(found).State == EntityState.Deleted)
                {
                    _logger.WriteDebug("Executing void Remove was in EntityState.Modified");
                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception exception)
            {
                _logger.WriteError(exception);
            }
        }

        /// <inheritdoc />
        public void Dispose()
        {
            _context?.Dispose();
        }
    }
}